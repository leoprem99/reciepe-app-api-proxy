# Reciepe App API Proxy

NGINX proxy app for our reciepe app API

## Usage

### Enviornment Variables
* `LISTEN_PORT` - Port to Listen on (default: `8000`)
* `APP_HOST` - Hostname of the of the app to forward requests to (default: `app`)
* `APP_PORT` - Port of the app to forward requests to (default: `9000`)